'use strict';

const path = require('path');

module.exports = dirs => {
  const pbem = {
    elementDelimiter: '-',
    modifierDelimiter: '--',
    modifierValueDelimiter: '-',
    vordsDelimiter: '',
  };

  const blocksGlob = path.join(dirs.blocks, '*');
  const elementsGlob = path.join(
    blocksGlob,
    `${pbem.elementDelimiter}!(${pbem.elementDelimiter})*`
  );
  const modGlobPart = `${pbem.modifierDelimiter}!(${pbem.modifierDelimiter})*`;
  const lessGlobPart = '*.less';

  return {
    pbem,
    bem: {
      blocksGlob,
      elementsGlob,
      modGlobPart,
      lessGlobPart,
    },
  };
};
