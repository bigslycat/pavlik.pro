'use strict';

function createTree(initial) {
  const result = {};

  initial.forEach((inputCurrent, inputCurrentIndex) => {
    const forwarded = Array.from(initial);
    forwarded.splice(inputCurrentIndex, 1);

    result[inputCurrent] = createTree(forwarded);
  });

  return result;
}

function mergeTree(tree, current = [], result = []) {
  const keys = Object.keys(tree);

  keys.forEach(element => {
    const forwarded = Array.from(current);
    forwarded.push(element);
    mergeTree(tree[element], forwarded, result);
  });

  if (keys.length === 0) {
    result.push(current);
  }

  return result;
}

const tree = createTree([
  'arrow-down',
  'telegram',
  'github',
  'email',
]);

console.log(mergeTree(tree));
