'use strict';

import goTo from './lib/goTo';

let loaded;

function onDomReady() {
  loaded = true;

  Array.from(document.querySelectorAll('.Link--goTo')).forEach(link =>
    link.addEventListener('click', event => {
      event.preventDefault();
      goTo(event.target, 10);
    })
  );
}

__load_polyfills__()(() => {
  document.addEventListener('DOMContentLoaded', onDomReady);
  if (document.readyState !== 'loading' && !loaded) onDomReady();
});
