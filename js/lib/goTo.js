'use strict';

import EasyGen from 'easygen';

const fps = 60;

const stepDuration = Math.round(1000 / fps);

const options = [{
  easing: [0.8, 0, 0, 0.95],
  duration: 1400,
}, {
  easing: [1, 0.05, 0.2, 1],
  duration: 1400,
}];

const countSteps = [];

options.forEach(part => countSteps.push(
  Math.round(part.duration / (1000 / fps))
));

const animation = [];

function makeStep(position) {
  const scroll = window.scroll.bind(window, 0, position);
  const frame = requestAnimationFrame.bind(window, scroll);

  animation.push(frame);
}

let animationHandler;
let currentFrame = 0;

function doStep() {
  const step = animation[currentFrame];

  if (!step) {
    clearInterval(animationHandler);
    animation.length = 0;
    currentFrame = 0;
    return;
  }

  step();
  currentFrame++;
}

export default (link, offset = 0) => {
  const targetId = link.hash.slice(1);
  const target = document.getElementById(targetId);

  const start = window.scrollY;
  const end = target.offsetTop + offset;
  const length = end - start;

  const easing = new EasyGen(start);

  const keyPositions = [
    start + (length / 2.5),
    end,
  ];

  options.forEach((part, index) => easing.addKey(
    part.easing,
    keyPositions[index]
  ));

  const frames = easing.build(...countSteps);

  frames.forEach(makeStep);

  animationHandler = setInterval(doStep, stepDuration);
};
