Я неплохо разбираюсь в клиентском и серверном JavaScript, слежу за развитием
спецификации и за внедрением новых возможностей. Это значит, что:

-   Я в курсе, что такое ES6, и как безопасно писать на нём клиентский код.

-   Я умею реализовывать модульность в JS, как синхронно, так и AMD.

-   Я привык использовать в работе Gulp, Webpack, Babel.

-   Я стремлюсь искать элегантные решения, использовать и изучать новые
    паттерны.

-   Я стараюсь, писать меньше кода и делать это качественно, делая упор на
    простоту дальнейших расширения и поддержки.

Помимо всего прочего, я имею опыт работы с Express, MongoDB native driver,
Mongoose и, jQuery и Leaflet, приветствую юнит-тестирование и переложение
максимума сложности на этап проектирования.

Я знаю HTML5, уделяю внимание семантике. В вёрстке я придерживаюсь методологии
БЭМ, ненавижу каскады и связанность. Там, где нужен шаблонизатор, предпочитаю
использовать Pug (в прошлом Jade), но, помимо него, знаю ещё и Ejs, Handlebars,
Twig.

Хорошо разбираюсь в CSS, использую LESS при написании кода, знаю, что такое
Flexbox, адаптивный дизайн, mobile-first и прочие интересные штуки.

Умею работать с Git и Mercurial. Знаю, что такое Git-flow и GitHub-flow, умею
адекватно применять оба подхода. Осваиваю Docker.

Ещё я знаю PHP и могу спокойно работать со стеком PHP+\*SQL.

### Open Source

-   [pbem](https://github.com/bigslycat/pbem) — Система БЭМ-хелперов для Pug.

-   [EasyGen](https://github.com/bigslycat/easygen) — Генератор дискретных
    значений из кривых Безье и их последовательностей. Пригодится для сложных
    easing в анимации и, возможно, кому-то будет удобно для отображения кривых.
    Написал, т.к. не хотел тянуть жирные библиотеки ради таких простых вещей.

Исходный код этого сайта
[здесь](https://bitbucket.org/bigslycat/pavlik.pro).
