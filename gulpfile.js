'use strict';

const fs = require('fs');
const path = require('path');
const yaml = require('yaml');

const gulp = require('gulp');

const less = require('gulp-less');
const concat = require('gulp-concat');
const mmq = require('gulp-merge-media-queries');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');

const pug = require('gulp-pug');
const pbem = require('pbem');
const htmlmin = require('gulp-htmlmin');

const glob = require('glob');
const syncy = require('syncy');
const markdown = require('markdown-it');

const webpack = require('webpack');

const webpackStream = require('webpack-stream');

const entries = require('object.entries');

if (!Object.entries) entries.shim();

const config = yaml.eval(fs.readFileSync('config.yml', 'utf8'));
const meta = yaml.eval(fs.readFileSync('meta.yml', 'utf8'));

const {
  dirs,
  markdown: markdownConfig,
  webpack: webpackConfig,
  viewport = {},
  autoprefix: autoprefixConfig,
  styles: { mainCssFileName, urls: styles = [] },
} = config;

const md = markdown(markdownConfig);

dirs.src = path.resolve(__dirname, dirs.src);
dirs.deploy = path.resolve(__dirname, dirs.deploy);
dirs.blocks = path.resolve(dirs.src, dirs.blocks);
dirs.views = path.resolve(dirs.src, dirs.views);
dirs.fonts = path.resolve(dirs.src, dirs.fonts);
dirs.content = path.resolve(dirs.src, dirs.content);

dirs.jsSrc = path.join(dirs.src, dirs.js);
dirs.jsDeploy = path.join(dirs.deploy, dirs.js);

dirs.stylesSrc = path.join(dirs.src, dirs.styles);
dirs.stylesDeploy = path.join(dirs.deploy, dirs.styles);

dirs.imagesDeploy = path.join(dirs.deploy, dirs.images);

const {
  pbem: pbemConst,
  bem: {
    blocksGlob,
    elementsGlob,
    modGlobPart,
    lessGlobPart,
  },
} = require('./const')(dirs);

function onError(error) {
  console.log(error);
}

const mainCssPath = path.join(dirs.stylesDeploy, mainCssFileName);

const stylesPath = path.join(dirs.stylesSrc, lessGlobPart);

function copyFile(source, target) {
  return new Promise((resolve, reject) => {
    const rd = fs.createReadStream(source);
    rd.on('error', reject);
    const wr = fs.createWriteStream(target);
    wr.on('error', reject);
    wr.on('finish', resolve);
    rd.pipe(wr);
  });
}

gulp.task('styles', done =>
  new Promise(resolve => glob(

    path.join(dirs.blocks, '*', dirs.images, '*'),
    (err, matches) => resolve(matches)

  )).then(matches => Promise.all([

    new Promise(resolve => fs.mkdir(
      dirs.imagesDeploy, resolve
    )).then(() => matches.map(filePath => copyFile(
      filePath,
      path.join(dirs.imagesDeploy, path.basename(filePath))
    ))),

    syncy(path.join(dirs.fonts, '**'), dirs.deploy, {
      base: dirs.src,
      updateAndDelete: false,
    }),

    new Promise(resolve => gulp.src([
      path.join(dirs.src, 'node_modules', 'normalize.css', 'normalize.css'),
      stylesPath,

      path.join(blocksGlob, lessGlobPart),
      path.join(blocksGlob, modGlobPart, lessGlobPart),

      path.join(elementsGlob, lessGlobPart),
      path.join(elementsGlob, modGlobPart, lessGlobPart),
    ])
    .pipe(sourcemaps.init())
    .pipe(concat(mainCssFileName))
    .pipe(less().on('error', onError))
    .pipe(mmq())
    .pipe(autoprefixer(autoprefixConfig))
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dirs.stylesDeploy))
    .on('end', resolve)),

  ])).then(done)
);

const bemScope = pbem(Object.assign({
  viewsDir: dirs.views,
  blocksDir: dirs.blocks,
}, pbemConst)).precompile();

styles.push(path.join('/', path.relative(dirs.deploy, mainCssPath)));

const resumeFileName = path.join(dirs.content, 'resume.md');

function getMd(filePath) {
  return (new Promise((resolve, reject) =>
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) reject(err);
      resolve(data);
    })
  )).then(data => md.render(data));
}

const basicLocals = Object.assign({
  viewport,
  block: bemScope.createBlock,
  styles,
}, meta);

gulp.task('templates', done => {
  const stream = gulp.src([
    path.join(dirs.views, 'index.pug'),
  ]);

  getMd(resumeFileName).then(content => {
    const locals = Object.assign({
      content,
      lastUpdate: (new Date()).toISOString(),
    }, basicLocals);

    stream.pipe(pug({ locals }).on('error', onError))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(dirs.deploy))
    .on('end', done);
  });
});

const PolyfillServicePlugin = require('polyfill-service-webpack');

webpackConfig.plugins = [
  new PolyfillServicePlugin({
    minify: true,
    callback: 'onPolyfillsLoaded',
    defaultFeatures: {},
    flags: [],
    libVersion: '>0.0.0',
  }),
  new webpack.optimize.UglifyJsPlugin(),
];

webpackConfig.output.publicPath = `/${path.relative(dirs.deploy, dirs.jsSrc)}/`;

gulp.task('app', () =>
  gulp.src(path.join(dirs.jsSrc, 'app.js'))
  .pipe(webpackStream(webpackConfig))
  .pipe(gulp.dest(dirs.jsDeploy))
);

function onChange(event) {
  console.log(`File ${event.path} was ${event.type}, running tasks...`);
}

gulp.watch([
  path.join(dirs.jsSrc, '**', '*.js'),
], [
  'app',
]).on('change', onChange);

gulp.watch([
  stylesPath,
  path.join(dirs.blocks, '**', lessGlobPart),
], [
  'styles',
]).on('change', onChange);

gulp.watch([
  path.join(dirs.content, '**', '*.md'),
  path.join(dirs.views, '**', '*.pug'),
  path.join(dirs.blocks, '**', '*.pug'),
], [
  'templates',
]).on('change', onChange);
